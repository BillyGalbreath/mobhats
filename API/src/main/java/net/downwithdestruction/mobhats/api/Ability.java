package net.downwithdestruction.mobhats.api;

import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

/**
 * Blank ability class
 * <p>
 * This ability class contains a blank method for every event listener
 * for all abilities. Each ability will extend this class and override
 * any event listeners it requires.
 */
public abstract class Ability {
    /**
     * Fired when a MobHAt entity causes block to form (snow from snowman, etc)
     *
     * @param event EntityBlockFormEvent
     */
    public void onEntityBlockForm(EntityBlockFormEvent event) {
    }

    /**
     * Event that fires when players change worlds while wearing a MobHat
     *
     * @param event PlayerChangedWorldEvent
     */
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
    }

    /**
     * Event that fires when player hits another entity while wearing a MobHat
     *
     * @param event EntityDamageByEntityEvent
     */
    public void onPlayerHitEntity(EntityDamageByEntityEvent event) {
    }

    /**
     * Event that fires when players interact while wearing a MobHat
     *
     * @param event PlayerInteractEvent
     */
    public void onPlayerInteract(PlayerInteractEvent event) {
    }

    /**
     * Event that fires when players wearing a MobHat move
     *
     * @param event PlayerMoveEvent
     */
    public void onPlayerMove(PlayerMoveEvent event) {
    }

    /**
     * Event that fires when players respawn while wearing a MobHat
     *
     * @param event PlayerRespawnEvent
     */
    public void onPlayerRespawn(PlayerRespawnEvent event) {
    }

    /**
     * Event that fires when players teleports while wearing a MobHat
     *
     * @param event PlayerTeleportEvent
     */
    public void onPlayerTeleport(PlayerTeleportEvent event) {
    }

    /**
     * Event that fires when players toggle their flight mode while wearing a MobHat
     *
     * @param event PlayerToggleFlightEvent
     */
    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
    }

    /**
     * Event that fires when players toggle sneak while wearing a MobHat
     *
     * @param event PlayerToggleSneakEvent
     */
    public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
    }

    /**
     * Event that fires when players take damage while wearing a MobHat
     *
     * @param event EntityDamageEvent
     */
    public void onPlayerReceiveDamage(EntityDamageEvent event) {
    }
}
