package net.downwithdestruction.mobhats.api;

import com.google.common.collect.Maps;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.WordUtils;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;

/**
 * Mob hat types
 */
public enum HatType {
    /**
     * Bat MobHat Type
     * <p>
     * Ability: Double Jump
     */
    BAT(EntityType.BAT, "Double Jump"),
    /**
     * Blaze MobHat Type
     * <p>
     * Ability: Shoot Fireballs
     */
    BLAZE(EntityType.BLAZE, "Shoot Fireballs"),
    /**
     * Cave Spider MobHat Type
     * <p>
     * Ability: Night Vision
     */
    CAVE_SPIDER(EntityType.CAVE_SPIDER, "Night Vision"),
    /**
     * Chicken MobHat Type
     * <p>
     * Ability:
     */
    CHICKEN(EntityType.CHICKEN, ""),
    /**
     * Cow MobHat Type
     * <p>
     * Ability:
     */
    COW(EntityType.COW, ""),
    /**
     * Creeper MobHat Type
     * <p>
     * Ability: Self Explode
     */
    CREEPER(EntityType.CREEPER, "Self Explode"),
    /**
     * Enderman MobHat Type
     * <p>
     * Ability: Teleport
     */
    ENDERMAN(EntityType.ENDERMAN, "Teleport"),
    /**
     * Iron Golem MobHat Type
     * <p>
     * Ability: 400% Health
     */
    IRON_GOLEM(EntityType.IRON_GOLEM, "400% Health"),
    /**
     * Mushroom Cow MobHat Type
     * <p>
     * Ability:
     */
    MOOSHROOM_COW(EntityType.MUSHROOM_COW, ""),
    /**
     * Ocelot MobHat Type
     * <p>
     * Ability: No Fall Damage
     */
    OCELOT(EntityType.OCELOT, Ocelot.Type.WILD_OCELOT, "No Fall Damage"),
    OCELOT_SIAMESE(EntityType.OCELOT, Ocelot.Type.SIAMESE_CAT, ""),
    OCELOT_TABBY(EntityType.OCELOT, Ocelot.Type.RED_CAT, ""),
    OCELOT_TUXEDO(EntityType.OCELOT, Ocelot.Type.BLACK_CAT, ""),
    /**
     * Pig MobHat Type
     * <p>
     * Ability:
     */
    PIG(EntityType.PIG, ""),
    /**
     * Sheep MobHat Type
     * <p>
     * Ability:
     */
    SHEEP(EntityType.SHEEP, ""),
    /**
     * Silverfish MobHat Type
     * <p>
     * Ability:
     */
    SILVERFISH(EntityType.SILVERFISH, ""),
    /**
     * Skeleton MobHat Type
     * <p>
     * Ability:
     */
    SKELETON(EntityType.SKELETON, ""),
    /**
     * Small Slime MobHat Type
     * <p>
     * Ability:
     */
    SLIME(EntityType.SLIME, 1, ""),
    /**
     * Medium Slime MobHat Type
     * <p>
     * Ability:
     */
    SLIME_MEDIUM(EntityType.SLIME, 2, ""),
    /**
     * Large Slime MobHat Type
     * <p>
     * Ability:
     */
    SLIME_LARGE(EntityType.SLIME, 4, ""),
    /**
     * Snowman MobHat Type
     * <p>
     * Ability:
     */
    SNOWMAN(EntityType.SNOWMAN, ""),
    /**
     * Spider MobHat Type
     * <p>
     * Ability:
     */
    SPIDER(EntityType.SPIDER, ""),
    /**
     * Squid MobHat Type
     * <p>
     * Ability: Underwater Breathing
     */
    SQUID(EntityType.SQUID, "Underwater Breathing"),
    /**
     * Wither Skeleton MobHat Type
     * <p>
     * Ability:
     */
    WITHER_SKELETON(EntityType.SKELETON, Skeleton.SkeletonType.WITHER, ""),
    /**
     * Zombie MobHat Type
     * <p>
     * Ability:
     */
    ZOMBIE(EntityType.ZOMBIE, ""),
    /**
     * Zombie Pigman MobHat Type
     * <p>
     * Ability:
     */
    ZOMBIE_PIGMAN(EntityType.PIG_ZOMBIE, "");

    private final EntityType type;
    private final Object typeData;
    private final String name;
    private final String hyphenedName;
    private final String ability;

    /**
     * Constructs HatType enum entry
     *
     * @param type    EntityType
     * @param ability Ability description
     */
    HatType(EntityType type, String ability) {
        this(type, null, ability);
    }

    /**
     * Constructs HatType enum entry
     *
     * @param type     EntityType
     * @param typeData Object for type specific data
     * @param ability  Ability description
     */
    HatType(EntityType type, Object typeData, String ability) {
        this.type = type;
        this.typeData = typeData;
        this.name = WordUtils.capitalize(name().replace("_", " ").toLowerCase());
        this.hyphenedName = name().replace("_", "-").toLowerCase();
        this.ability = ability;
    }

    /**
     * Get the Bukkit EntityType of this hat
     *
     * @return EntityType
     */
    public EntityType getEntityType() {
        return type;
    }

    /**
     * Get the type specific data
     *
     * @return Object
     */
    public Object getTypeData() {
        return typeData;
    }

    /**
     * Get the hat type name
     *
     * @return Hat type name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the hat type name with underscores replaced by hyphens
     *
     * @return Hat type name
     */
    public String getHyphenedName() {
        return hyphenedName;
    }

    /**
     * Get the ability description
     *
     * @return Ability description
     */
    public String getAbilityDescription() {
        return ability;
    }

    /**
     * Check if player has permission to wear this hat type
     *
     * @param player Player
     * @return True if player has permission
     */
    public boolean hasPermission(Player player) {
        return player.hasPermission("command.mobhat." + getName().toLowerCase());
    }

    /**
     * Get HatType by name
     *
     * @param name Name of hat type
     * @return HatType, or null if no type exists by name specified
     */
    public static HatType getEntityType(String name) {
        Validate.notNull(name, "Name cannot be null");

        return BY_NAME.get(name.toUpperCase(Locale.ENGLISH)
                .replaceAll("\\-", "_")
                .replaceAll("\\s+", "_")
                .replaceAll("\\W", ""));
    }

    private final static Map<String, HatType> BY_NAME = Maps.newHashMap();

    static {
        for (HatType type : values()) {
            BY_NAME.put(type.name(), type);
        }
    }
}
