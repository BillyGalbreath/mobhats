package net.downwithdestruction.mobhats.api;

import net.downwithdestruction.mobhats.api.manager.HatManager;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Represents a MobHat
 */
public abstract class MobHat {
    private Entity holder;
    private Entity entity;
    private Ability ability;
    private final HatType type;
    private final Player player;

    /**
     * Constructs a new MobHat with given HatType for Player
     *
     * @param type   HatType
     * @param player Player
     */
    public MobHat(HatType type, Player player) {
        this.type = type;
        this.player = player;

        HatManager.getManager().spawnEntities(this);
    }

    /**
     * Set the MobHat ability listener
     *
     * @param ability Ability
     */
    public void setAbility(Ability ability) {
        this.ability = ability;
    }

    /**
     * Set the MobHat holder entity
     *
     * @param holder Holder entity
     */
    public void setHolder(Entity holder) {
        this.holder = holder;
    }

    /**
     * Set the MobHat entity
     *
     * @param entity Entity
     */
    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    /**
     * Get the holder entity for this hat
     * <p>
     * This is the entity between the player and the hat (pig)
     *
     * @return Hat holder entity
     */
    public Entity getHolder() {
        return holder;
    }

    /**
     * Get the mob hat entity
     *
     * @return Entity of mob hat
     */
    public Entity getEntity() {
        return entity;
    }

    /**
     * Get the hat type
     *
     * @return HatType
     */
    public HatType getType() {
        return type;
    }

    /**
     * Get the player
     *
     * @return Player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Get the MobHat Ability
     *
     * @return Ability
     */
    public Ability getAbility() {
        return ability;
    }

    /**
     * Called when the MobHat is attached to Player's head
     */
    public void onAttach() {
    }

    /**
     * Called when the MobHat is removed from Player's head
     */
    public void onDetach() {
    }
}
