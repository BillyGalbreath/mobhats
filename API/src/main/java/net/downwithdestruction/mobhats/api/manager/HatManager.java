package net.downwithdestruction.mobhats.api.manager;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * MobHat Manager
 */
public abstract class HatManager {
    private static HatManager instance;

    /**
     * Get the HatManager instance
     *
     * @return HatManager
     */
    public static HatManager getManager() {
        return instance;
    }

    /**
     * Creates new HatManager instance
     */
    protected HatManager() {
        HatManager.instance = this;
    }

    protected final Map<UUID, MobHat> mobHats = new HashMap<>();

    /**
     * Set a player's MobHat (removes old hat if present)
     *
     * @param player  Player
     * @param hatType HatType
     */
    public abstract void setHat(Player player, HatType hatType);

    /**
     * Remove a player's MobHat
     *
     * @param player Player
     */
    public abstract void removeHat(Player player);

    /**
     * Remove all hats from world and memory
     */
    public abstract void removeAll();

    /**
     * Get a player's MobHat
     *
     * @param player Player
     * @return MobHat player is wearing, or null
     */
    public abstract MobHat getHat(Player player);

    /**
     * Get a MobHat from entity or holder
     *
     * @param entity Entity
     * @return MobHat if entity is the hat or holder, or null
     */
    public abstract MobHat getHat(Entity entity);

    /**
     * Spawn the MobHat entities in the world and attach to player
     *
     * @param mobHat MobHat
     */
    public abstract void spawnEntities(MobHat mobHat);

    /**
     * Despawn the MobHat entities but leave the hat set
     *
     * @param mobHat MobHat
     */
    public abstract void despawnEntities(MobHat mobHat);

    /**
     * Spawn a MobHat on a player
     *
     * @param hatType HatType to spawn
     * @param player  Player to spawn on
     * @return The spawned MobHat
     */
    public abstract MobHat spawnHat(HatType hatType, Player player);
}
