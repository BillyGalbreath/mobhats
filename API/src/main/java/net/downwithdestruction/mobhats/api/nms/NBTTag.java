package net.downwithdestruction.mobhats.api.nms;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

/**
 * NBTTag utility
 */
public abstract class NBTTag {
    private static NBTTag instance;

    /**
     * Get the NBTTag instance
     *
     * @return NBTTag
     */
    public static NBTTag getInstance() {
        return instance;
    }

    /**
     * Create new NBTTag instance
     */
    protected NBTTag() {
        NBTTag.instance = this;
    }

    /**
     * Set NBT for a Bukkit Entity on or off
     *
     * @param entity  Bukkit Entity
     * @param tagName Name of NBT tag
     * @param value   Boolean value of tag
     */
    public abstract void setTag(Entity entity, String tagName, boolean value);

    /**
     * Set NBT for a ItemStack monster egg
     *
     * @param item  ItemStack
     * @param value EntityType
     * @return new ItemStack
     */
    public abstract ItemStack setTag(ItemStack item, EntityType value);

    /**
     * Get NBT for a Bukkit Entity
     *
     * @param entity  Entity
     * @param tagName Name of NBT tag
     * @return Boolean value of NBT tag
     */
    public abstract Boolean getTag(Entity entity, String tagName);
}
