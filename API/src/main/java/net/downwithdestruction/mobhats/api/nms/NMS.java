package net.downwithdestruction.mobhats.api.nms;

import org.bukkit.entity.Entity;

/**
 * NMS Utility
 */
public abstract class NMS {
    private static NMS instance;

    /**
     * Get NMS instance
     *
     * @return NMS
     */
    public static NMS getInstance() {
        return instance;
    }

    /**
     * Create new NMS instance
     */
    protected NMS() {
        NMS.instance = this;
    }

    /**
     * Set entity as silent
     *
     * @param passenger Entity
     */
    public abstract void setSilent(Entity passenger);

    /**
     * Set entity as invisible
     *
     * @param passenger Entity
     */
    public abstract void setInvisible(Entity passenger);

    /**
     * Set entity rotation
     *
     * @param passenger Entity
     * @param yaw       Rotation
     */
    public abstract void setRotation(Entity passenger, float yaw);

    /**
     * Remove entity's collision box
     *
     * @param entity Entity
     */
    public abstract void removeCollisionBox(Entity entity);
}
