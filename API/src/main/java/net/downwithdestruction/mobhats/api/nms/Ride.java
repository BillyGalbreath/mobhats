package net.downwithdestruction.mobhats.api.nms;

import org.bukkit.entity.Entity;

/**
 * Ride/Passenger Utility
 * Workaround for 1.9+ not handling passengers correctly
 * See SPIGOT-1915 for details (md_5 blames Mojang bug)
 */
public abstract class Ride {
    private static Ride instance;

    public static Ride getInstance() {
        return instance;
    }

    protected Ride() {
        Ride.instance = this;
    }

    /**
     * Place passenger on a vehicle
     *
     * @param vehicle   Vehicle
     * @param passenger Passenger
     */
    public abstract void startRiding(Entity vehicle, Entity passenger);

    /**
     * Remove passenger from any vehicle
     *
     * @param passenger Passenger
     */
    public abstract void stopRiding(Entity passenger);

    /**
     * Eject vehicle's passengers
     *
     * @param vehicle Vehicle
     */
    public abstract void ejectPassengers(Entity vehicle);
}
