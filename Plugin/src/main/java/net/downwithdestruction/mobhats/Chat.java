package net.downwithdestruction.mobhats;

import net.downwithdestruction.mobhats.configuration.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Represents a chat message sent to CommandSender
 */
public class Chat {
    private final String message;

    /**
     * New chat message to send
     *
     * @param lang Lang entry
     */
    public Chat(Lang lang) {
        this(lang.toString());
    }

    /**
     * New chat message to send
     *
     * @param message Message
     */
    public Chat(String message) {
        this.message = ChatColor.translateAlternateColorCodes('&', message);
    }

    /**
     * Send message to CommandSender
     * <p>
     * Will not send any blank messages
     *
     * @param recipient Recipient of message
     */
    public void send(CommandSender recipient) {
        if (message == null || ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
