package net.downwithdestruction.mobhats;

import net.downwithdestruction.mobhats.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * Logger utility class
 */
public class Logger {
    private static void log(String msg) {
        msg = ChatColor.translateAlternateColorCodes('&', "&3[&d" + MobHats.getPlugin().getName() + "&3]&r " + msg);
        if (!Config.SETTINGS__COLOR_LOGS.getBoolean()) {
            msg = ChatColor.stripColor(msg);
        }
        Bukkit.getServer().getConsoleSender().sendMessage(msg);
    }

    /**
     * Output debug message to console
     *
     * @param msg Message
     */
    public static void debug(String msg) {
        if (Config.SETTINGS__DEBUG_MODE.getBoolean()) {
            Logger.log("&7[&eDEBUG&7]&e " + msg);
        }
    }

    /**
     * Output warning message to console
     *
     * @param msg Message
     */
    public static void warn(String msg) {
        Logger.log("&e[&6WARN&e]&6 " + msg);
    }

    /**
     * Output error message to console
     *
     * @param msg Message
     */
    public static void error(String msg) {
        Logger.log("&e[&4ERROR&e]&4 " + msg);
    }

    /**
     * Output information message to console
     *
     * @param msg Message
     */
    public static void info(String msg) {
        Logger.log("&e[&fINFO&e]&r " + msg);
    }
}
