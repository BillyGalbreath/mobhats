package net.downwithdestruction.mobhats;

import co.insou.gui.GUIManager;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import net.downwithdestruction.mobhats.command.CmdMobHat;
import net.downwithdestruction.mobhats.configuration.Lang;
import net.downwithdestruction.mobhats.listener.AbilityListener;
import net.downwithdestruction.mobhats.listener.MobHatListener;
import net.downwithdestruction.mobhats.listener.PlayerListener;
import net.downwithdestruction.mobhats.manager.HatManagerImpl;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main plugin class
 */
public class MobHats extends JavaPlugin {
    private static GUIManager guiManager;

    /**
     * Get MobHats plugin instance
     *
     * @return MobHats plugin instance
     */
    public static MobHats getPlugin() {
        return MobHats.getPlugin(MobHats.class);
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        String packageName = this.getServer().getClass().getPackage().getName();
        String nms = "net.downwithdestruction.mobhats.nms." + packageName.substring(packageName.lastIndexOf('.') + 1);

        try {
            // We just need to create instances of these classes. API holds their instances
            Class.forName(nms + ".NBTTagHandler").getConstructor().newInstance();
            Class.forName(nms + ".NMSHandler").getConstructor().newInstance();
            Class.forName(nms + ".RideHandler").getConstructor().newInstance();
        } catch (Exception e) {
            Logger.warn(e.getLocalizedMessage());
            Logger.warn("Could not find support for this server version!");
            Logger.warn("Disabling plugin.");
            return;
        }

        guiManager = new GUIManager(this);
        new HatManagerImpl();

        getServer().getPluginManager().registerEvents(new AbilityListener(), this);
        getServer().getPluginManager().registerEvents(new MobHatListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        getCommand("mobhat").setExecutor(new CmdMobHat());

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        if (HatManager.getManager() != null) {
            HatManager.getManager().removeAll();
        }

        Logger.info(getName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.RED + getName() + " is disabled. Please check console logs for more information.");
        return true;
    }

    /**
     * Get GUIManager instance
     *
     * @return GUIManager instance
     */
    public static GUIManager getGUIManager() {
        return guiManager;
    }
}
