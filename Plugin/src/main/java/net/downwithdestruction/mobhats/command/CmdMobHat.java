package net.downwithdestruction.mobhats.command;

import co.insou.gui.GUIPlayer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import net.downwithdestruction.mobhats.configuration.Config;
import net.downwithdestruction.mobhats.configuration.Lang;
import net.downwithdestruction.mobhats.gui.HatGUI;
import net.pl3x.bukkit.titleapi.Title;
import net.pl3x.bukkit.titleapi.api.TitleType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class CmdMobHat implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> result = new ArrayList<>();
        if (args.length == 1) {
            String arg = args[0].toLowerCase();
            result.addAll(Arrays.asList("remove", "reload").stream()
                    .filter(cmd -> cmd.toLowerCase().startsWith(arg))
                    .collect(Collectors.toList()));
            result.addAll(Arrays.asList(HatType.values()).stream()
                    .filter(type -> type.getHyphenedName().startsWith(arg))
                    .map(HatType::getHyphenedName)
                    .collect(Collectors.toList()));
        }
        return result;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Lang.PLAYER_COMMAND.toString());
            return true;
        }

        Player player = (Player) sender;

        if (!sender.hasPermission("command.mobhat")) {
            sender.sendMessage(Lang.COMMAND_NO_PERMISSION.toString());
            return true;
        }

        if (args.length == 0) {
            GUIPlayer guiPlayer = MobHats.getGUIManager().getPlayer(player);
            guiPlayer.openPage(new HatGUI(guiPlayer), true);
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            Config.reload();
            Lang.reload(true);

            // TODO hat persistence?

            sender.sendMessage(Lang.RELOAD
                    .replace("{plugin}", MobHats.getPlugin().getName())
                    .replace("{version}", MobHats.getPlugin().getDescription().getVersion()));
            return true;
        }

        if (args[0].equalsIgnoreCase("remove")) {
            HatManager.getManager().removeHat(player);
            sender.sendMessage(Lang.HAT_REMOVED.toString());
            return true;
        }

        HatType hatType = HatType.getEntityType(args[0]);
        if (hatType == null) {
            sender.sendMessage(Lang.UNKNOWN_HAT_TYPE.toString());
            return true;
        }

        if (!hatType.hasPermission(player)) {
            new Title(TitleType.RESET, null)
                    .send(player);
            new Title(Config.TITLE__FADE_IN.getInt(),
                    Config.TITLE__STAY.getInt(),
                    Config.TITLE__FADE_OUT.getInt())
                    .send(player);
            new Title(TitleType.TITLE, Lang.HAT_RESTRICTED_TITLE
                    .replace("{player}", player.getName())
                    .replace("{type}", hatType.getName())
                    .replace("{ability}", hatType.getAbilityDescription()))
                    .send(player);
            new Title(TitleType.SUBTITLE, Lang.HAT_RESTRICTED_SUBTITLE
                    .replace("{player}", player.getName())
                    .replace("{type}", hatType.getName())
                    .replace("{ability}", hatType.getAbilityDescription()))
                    .send(player);
            return true;
        }

        HatManager.getManager().setHat(player, hatType);
        return true;
    }
}
