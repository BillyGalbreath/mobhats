package net.downwithdestruction.mobhats.configuration;

import net.downwithdestruction.mobhats.MobHats;

public enum Config {
    /**
     * Boolean for console colors
     */
    SETTINGS__COLOR_LOGS(true),
    /**
     * Boolean for console debug mode
     */
    SETTINGS__DEBUG_MODE(false),
    /**
     * String filename for language file
     */
    SETTINGS__LANGUAGE_FILE("lang-en.yml"),
    /**
     * Title fade in time (ticks)
     */
    TITLE__FADE_IN(10),
    /**
     * Title stay time (ticks)
     */
    TITLE__STAY(60),
    /**
     * Title fade out time (ticks)
     */
    TITLE__FADE_OUT(20),
    /**
     * Bat double jump direction multiplier
     */
    ABILITY__BAT__DIRECTION_MULTIPLIER(1.0D),
    /**
     * Bat double jump height velocity
     */
    ABILITY__BAT__HEIGHT_MULTIPLIER(1.0D),
    /**
     * How long the bat stays awake when double jumping (in ticks)
     */
    ABILITY__BAT__AWAKE_TIME(20),
    /**
     * Blaze fireball velocity speed multiplier
     */
    ABILITY__BLAZE__SPEED(2.0D),
    /**
     * Blaze fireball cooldown (in ticks)
     */
    ABILITY__BLAZE__COOLDOWN(20),
    /**
     * Creeper explode delay (30 ticks is vanilla delay)
     */
    ABILITY__CREEPER__DELAY(30),
    /**
     * Creeper explode power (3.0D is vanilla creeper power)
     */
    ABILITY__CREEPER__POWER(3.0D),
    /**
     * Creeper explode blink rate
     */
    ABILITY__CREEPER__BLINK_DELAY(4),
    /**
     * Creeper explode create fire
     */
    ABILITY__CREEPER__FIRE(false),
    /**
     * Creeper explode play fuse/hiss sound
     */
    ABILITY__CREEPER__SOUND(true),
    /**
     * Enderman teleport max distance
     */
    ABILITY__ENDERMAN__MAX_TELEPORT_DISTANCE(50),
    /**
     * Iron golem max health
     */
    ABILITY__IRON_GOLEM__MAX_HEALTH(80.0D),
    /**
     * Whether snowman hat leaves snow trail on ground or not
     */
    ABILITY__SNOWMAN__LEAVE_TRAIL(false);

    private final MobHats plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = MobHats.getPlugin();
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("__", ".").replace("_", "-");
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    public double getDouble() {
        return plugin.getConfig().getDouble(getKey(), (double) def);
    }

    public float getFloat() {
        return (float) getDouble();
    }

    public static void reload() {
        MobHats.getPlugin().reloadConfig();
    }
}
