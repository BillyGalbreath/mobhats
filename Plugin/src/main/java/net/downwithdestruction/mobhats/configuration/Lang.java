package net.downwithdestruction.mobhats.configuration;

import java.io.File;
import net.downwithdestruction.mobhats.Logger;
import net.downwithdestruction.mobhats.MobHats;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public enum Lang {
    COMMAND_NO_PERMISSION("&4You do not have permission for this command!"),
    PLAYER_COMMAND("&4Player only command!"),
    UNKNOWN_HAT_TYPE("&4Unknown hat type!"),
    CANT_SET_HAT_HERE("&4Can't set a hat here!\n&4Please move to a different location and try again!"),

    HAT_ATTACHED_TITLE("&6Mob Hat Attached"),
    HAT_ATTACHED_SUBTITLE("&bType: &e{type}"),
    HAT_ATTACHED_ACTIONBAR("&2&l{player}&2, you have selected Mob Hat &6{type}"),
    HAT_ATTACHED_CHAT("&6Mob Hat &7{type} &6attached."),

    HAT_RESTRICTED_TITLE("&6Sorry"),
    HAT_RESTRICTED_SUBTITLE("&4This Mob Hat is restricted, try again!"),

    HAT_REMOVED("&dMob hat removed."),

    GUI_TITLE("&1&oMob Hats:"),
    GUI_ITEM_ABILITY("&b{ability}"),
    GUI_NO_PERMS("&cSorry &b{player} &c, You do\n&cnot have permissions to use\n&cthis Mob Hat!"),

    GUI_TNT_NAME("&4Remove Your Mob Hat"),
    GUI_TNT_LORE("&6Clicking this &cTNT &6item\n&6will remove your Mob Hat."),
    GUI_REDSTONE_NAME("&4Close Mob Hat Menu"),
    GUI_REDSTONE_LORE("&6Quick way of closing\n&6your Mob Hat menu."),

    RELOAD("&d{plugin} v {version} reloaded.");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.SETTINGS__LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(MobHats.getPlugin().getDataFolder(), lang);
            if (!configFile.exists()) {
                MobHats.getPlugin().saveResource(Config.SETTINGS__LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
