package net.downwithdestruction.mobhats.gui;

import java.util.Arrays;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.nms.NBTTag;
import net.downwithdestruction.mobhats.configuration.Lang;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Utility for GUI items
 */
public class GUIItems {
    final static ItemStack AIR_STACK = new ItemStack(Material.AIR);
    final static ItemStack TNT_STACK = new ItemStack(Material.TNT);
    final static ItemStack REDSTONE_STACK = new ItemStack(Material.REDSTONE_BLOCK);
    private final static ItemStack MONSTER_EGG_STACK = new ItemStack(Material.MONSTER_EGG);

    static {
        ItemMeta meta = TNT_STACK.getItemMeta();
        meta.setDisplayName(Lang.GUI_TNT_NAME.toString());
        meta.setLore(Arrays.asList(Lang.GUI_TNT_LORE.toString().split("\n")));
        TNT_STACK.setItemMeta(meta);

        meta = REDSTONE_STACK.getItemMeta();
        meta.setDisplayName(Lang.GUI_REDSTONE_NAME.toString());
        meta.setLore(Arrays.asList(Lang.GUI_REDSTONE_LORE.toString().split("\n")));
        REDSTONE_STACK.setItemMeta(meta);
    }

    /**
     * Gets a monster egg itemstack for use in GUI menu
     *
     * @param hatType HatType for GUI entry
     * @param player  Player viewing GUI
     * @return ItemStack
     */
    static ItemStack getEggStack(HatType hatType, Player player) {
        ItemStack egg = NBTTag.getInstance().setTag(MONSTER_EGG_STACK, hatType.getEntityType());
        ItemMeta meta = egg.getItemMeta();
        if (hatType.hasPermission(player)) {
            meta.setDisplayName(ChatColor.GREEN + hatType.getName());
            meta.setLore(Arrays.asList(Lang.GUI_ITEM_ABILITY
                    .replace("{ability}", hatType.getAbilityDescription())
                    .split("\n")));
        } else {
            meta.setDisplayName(ChatColor.DARK_RED + hatType.getName());
            meta.setLore(Arrays.asList(Lang.GUI_NO_PERMS
                    .replace("{player}", player.getName())
                    .split("\n")));
        }
        egg.setItemMeta(meta);
        return egg;
    }
}
