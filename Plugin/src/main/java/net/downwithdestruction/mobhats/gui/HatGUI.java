package net.downwithdestruction.mobhats.gui;

import co.insou.gui.GUIPlayer;
import co.insou.gui.page.GUIInventory;
import co.insou.gui.page.GUIPage;
import co.insou.gui.page.PageInventory;
import net.downwithdestruction.mobhats.Logger;
import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import net.downwithdestruction.mobhats.configuration.Config;
import net.downwithdestruction.mobhats.configuration.Lang;
import net.pl3x.bukkit.titleapi.Title;
import net.pl3x.bukkit.titleapi.api.TitleType;
import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * The MobHat GUI
 */
public class HatGUI extends GUIPage<MobHats> {
    public HatGUI(GUIPlayer player) {
        super(MobHats.getPlugin(), player, Lang.GUI_TITLE.toString());
    }

    @Override
    protected GUIInventory loadInventory() {
        PageInventory inventory = new PageInventory(player, title);

        // set all the mob hat items
        int count = 0;
        for (HatType hatType : HatType.values()) {
            inventory.addItem(GUIItems.getEggStack(hatType, player.player()));
            count++;
        }
        // finish off the current row with air
        int remainder = 9 - (count % 9);
        for (int i = 0; i < remainder; i++) {
            inventory.addItem(GUIItems.AIR_STACK);
        }

        // set the remove hat control
        inventory.addItem(GUIItems.TNT_STACK);

        // separate controls with air
        for (int i = 0; i < 7; i++) {
            inventory.addItem(GUIItems.AIR_STACK);
        }

        // set the close gui control
        inventory.addItem(GUIItems.REDSTONE_STACK);

        return inventory;
    }

    @Override
    public void onInventoryClick(InventoryClickEvent event) {
        ItemStack item = event.getInventory().getItem(event.getSlot());

        // ignore null and air entries
        if (item == null || item.equals(GUIItems.AIR_STACK)) {
            return;
        }

        // close gui control
        if (item.equals(GUIItems.REDSTONE_STACK)) {
            player.closeGUI(true);
            return;
        }

        // remove hat control
        if (item.equals(GUIItems.TNT_STACK)) {
            player.closeGUI(true);
            HatManager.getManager().removeHat(player.player());
            player.player().sendMessage(Lang.HAT_REMOVED.toString());
            return;
        }

        // check displayname for known hat
        if (!item.hasItemMeta()) {
            return; // item has no meta. ignore
        }
        ItemMeta meta = item.getItemMeta();
        if (!meta.hasDisplayName()) {
            return; // does not have a display name. ignore
        }
        String displayName = ChatColor.stripColor(meta.getDisplayName()).trim();
        HatType hatType = HatType.getEntityType(displayName);
        if (hatType == null) {
            Logger.debug("Unable to find mobhat with name: " + displayName);
            return; // could not find hat type. ignore
        }

        if (!hatType.hasPermission(player.player())) {
            new Title(TitleType.RESET, null)
                    .send(player.player());
            new Title(Config.TITLE__FADE_IN.getInt(),
                    Config.TITLE__STAY.getInt(),
                    Config.TITLE__FADE_OUT.getInt())
                    .send(player.player());
            new Title(TitleType.TITLE, Lang.HAT_RESTRICTED_TITLE
                    .replace("{player}", player.player().getName())
                    .replace("{type}", hatType.getName())
                    .replace("{ability}", hatType.getAbilityDescription()))
                    .send(player.player());
            new Title(TitleType.SUBTITLE, Lang.HAT_RESTRICTED_SUBTITLE
                    .replace("{player}", player.player().getName())
                    .replace("{type}", hatType.getName())
                    .replace("{ability}", hatType.getAbilityDescription()))
                    .send(player.player());
            return; // no hat perms
        }

        // close gui and set hat
        player.closeGUI(true);
        HatManager.getManager().setHat(player.player(), hatType);
    }
}
