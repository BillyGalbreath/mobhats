package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

/**
 * Represents a Bat type MobHat
 */
public class BatHat extends MobHat {
    /**
     * Constructs a new Bat type MobHat for player
     *
     * @param player Player
     */
    public BatHat(Player player) {
        super(HatType.BAT, player);
        setAbility(new BatAbility());
    }

    /**
     * Bat ability class.
     * <p>
     * This class holds the event listeners for the BatHat
     */
    private class BatAbility extends Ability {
        /**
         * Allow flight toggle for player if on ground
         */
        @Override
        public void onPlayerMove(PlayerMoveEvent event) {
            Player player = event.getPlayer();
            if (player.getAllowFlight()) {
                return; // player already has allowed flight
            }

            if (player.getGameMode() == GameMode.CREATIVE) {
                return; // ignore creative players
            }

            if (player.isFlying()) {
                return; // ignore flying players
            }

            // TODO find better way for ground check
            //if (!NBTTag.getInstance().getTag(player, "OnGround")) {
            if (player.getLocation().subtract(0, 1, 0).getBlock().getType() == Material.AIR) {
                return; // player is not on ground
            }

            player.setAllowFlight(true);
        }

        /**
         * Double jump activated
         */
        @Override
        public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
            Player player = event.getPlayer();
            if (player.getGameMode() == GameMode.CREATIVE) {
                return; // ignore creative players
            }

            event.setCancelled(true);

            player.setAllowFlight(false);
            player.setFlying(false);

            player.setVelocity(player.getLocation().getDirection().normalize()
                    .multiply(Config.ABILITY__BAT__DIRECTION_MULTIPLIER.getDouble() / 2D)
                    .setY(Config.ABILITY__BAT__HEIGHT_MULTIPLIER.getDouble() / 2D));

            ((Bat) getEntity()).setAwake(true);
            Bukkit.getScheduler().runTaskLater(MobHats.getPlugin(),
                    () -> ((Bat) getEntity()).setAwake(false),
                    Config.ABILITY__BAT__AWAKE_TIME.getInt());
        }
    }
}
