package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Represents a Blaze type MobHat
 */
public class BlazeHat extends MobHat {
    /**
     * Constructs a new Blaze type MobHat for player
     *
     * @param player Player
     */
    public BlazeHat(Player player) {
        super(HatType.BLAZE, player);
        setAbility(new BlazeAbility());
    }

    /**
     * Blaze ability class.
     * <p>
     * This class holds the event listeners for the BlazeHat
     */
    private class BlazeAbility extends Ability {
        private boolean onCooldown = false;

        @Override
        public void onPlayerInteract(PlayerInteractEvent event) {
            if (event.getAction() != Action.LEFT_CLICK_BLOCK && event.getAction() != Action.LEFT_CLICK_AIR) {
                return; // not left clicking
            }

            shoot();
        }

        @Override
        public void onPlayerHitEntity(EntityDamageByEntityEvent event) {
            shoot();

            event.setCancelled(true);
        }

        /**
         * Player shoot a fireball
         */
        private void shoot() {
            if (onCooldown) {
                return;
            }

            Location eye = getPlayer().getEyeLocation();
            Location loc = eye.add(eye.getDirection().multiply(1.2));
            Fireball fireball = (Fireball) loc.getWorld().spawnEntity(loc, EntityType.FIREBALL);
            fireball.setVelocity(loc.getDirection().normalize()
                    .multiply(Config.ABILITY__BLAZE__SPEED.getDouble()));
            fireball.setShooter(getPlayer());

            onCooldown = true;
            Bukkit.getScheduler().runTaskLater(MobHats.getPlugin(), () -> onCooldown = false,
                    Config.ABILITY__BLAZE__COOLDOWN.getInt());
        }
    }
}
