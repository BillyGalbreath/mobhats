package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Represents a Cave Spider type MobHat
 */
public class CaveSpiderHat extends MobHat {
    private final PotionEffect NIGHT_VISION = new PotionEffect(
            PotionEffectType.NIGHT_VISION,
            Integer.MAX_VALUE,
            Integer.MAX_VALUE,
            false,
            false);

    /**
     * Constructs a new Cave Spider type MobHat for player
     *
     * @param player Player
     */
    public CaveSpiderHat(Player player) {
        super(HatType.CAVE_SPIDER, player);
        setAbility(new CaveSpiderAbility());
    }

    @Override
    public void onAttach() {
        getPlayer().addPotionEffect(NIGHT_VISION);
    }

    @Override
    public void onDetach() {
        getPlayer().removePotionEffect(PotionEffectType.NIGHT_VISION);
    }

    /**
     * Cave Spider ability class.
     * <p>
     * This class holds the event listeners for the CaveSpiderHat
     */
    private class CaveSpiderAbility extends Ability {
        @Override
        public void onPlayerRespawn(PlayerRespawnEvent event) {
            Bukkit.getScheduler().runTaskLater(MobHats.getPlugin(),
                    () -> event.getPlayer().addPotionEffect(NIGHT_VISION), 5);
        }

        @Override
        public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
            Bukkit.getScheduler().runTaskLater(MobHats.getPlugin(),
                    () -> event.getPlayer().addPotionEffect(NIGHT_VISION), 10);
        }
    }
}
