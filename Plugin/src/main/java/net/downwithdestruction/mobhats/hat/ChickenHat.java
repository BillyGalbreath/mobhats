package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Chicken type MobHat
 */
public class ChickenHat extends MobHat {
    /**
     * Constructs a new Chicken type MobHat for player
     *
     * @param player Player
     */
    public ChickenHat(Player player) {
        super(HatType.CHICKEN, player);
        setAbility(new ChickenAbility());
    }

    /**
     * Chicken ability class.
     * <p>
     * This class holds the event listeners for the ChickenHat
     */
    private class ChickenAbility extends Ability {
        //
    }
}
