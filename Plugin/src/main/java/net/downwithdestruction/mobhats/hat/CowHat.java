package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Cow type MobHat
 */
public class CowHat extends MobHat {
    /**
     * Constructs a new Cow type MobHat for player
     *
     * @param player Player
     */
    public CowHat(Player player) {
        super(HatType.COW, player);
        setAbility(new CowAbility());
    }

    /**
     * Cow ability class.
     * <p>
     * This class holds the event listeners for the CowHat
     */
    private class CowAbility extends Ability {
        //
    }
}
