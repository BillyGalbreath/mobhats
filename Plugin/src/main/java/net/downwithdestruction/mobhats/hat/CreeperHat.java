package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import net.downwithdestruction.mobhats.configuration.Config;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Represents a Creeper type MobHat
 */
public class CreeperHat extends MobHat {
    /**
     * Constructs a new Creeper type MobHat for player
     *
     * @param player Player
     */
    public CreeperHat(Player player) {
        super(HatType.CREEPER, player);
        setAbility(new CreeperAbility());
    }

    /**
     * Creeper ability class.
     * <p>
     * This class holds the event listeners for the CreeperHat
     */
    private class CreeperAbility extends Ability {
        private Explode explodeTask;

        @Override
        public void onPlayerMove(PlayerMoveEvent event) {
            // check for running explosion task
            if (explodeTask == null) {
                return; // no explosion task running
            }

            // check for actual movement
            Location to = event.getTo();
            Location from = event.getFrom();
            if (to.getX() == from.getX() && to.getY() == from.getY() && to.getZ() == from.getZ()) {
                return; // did not actually move
            }

            // cancel task because movement detected
            explodeTask.cancel();
        }

        @Override
        public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
            if (event.isSneaking()) {
                explodeTask = new Explode(event.getPlayer());
                explodeTask.runTaskLater(
                        MobHats.getPlugin(),
                        Config.ABILITY__CREEPER__DELAY.getInt());
                return;
            }

            if (explodeTask != null) {
                explodeTask.cancel();
            }
        }
    }

    /**
     * Explode the player and remove the hat
     */
    private class Explode extends BukkitRunnable {
        private final Player player;
        private final MobHat mobHat;
        private Blink blinkTask;

        Explode(Player player) {
            this.player = player;
            this.mobHat = HatManager.getManager().getHat(player);

            if (Config.ABILITY__CREEPER__SOUND.getBoolean()) {
                player.getWorld().playSound(
                        player.getLocation(),
                        Sound.ENTITY_CREEPER_PRIMED,
                        1.0F, 1.0F);
            }

            blinkTask = new Blink(player);
            blinkTask.runTaskTimer(MobHats.getPlugin(),
                    Config.ABILITY__CREEPER__BLINK_DELAY.getInt(),
                    Config.ABILITY__CREEPER__BLINK_DELAY.getInt());
        }

        @Override
        public void run() {
            // always cancel to clear memory
            cancel();

            // make sure player is still valid
            if (player == null || !player.isOnline()) {
                return;
            }

            // explode player
            player.getWorld().createExplosion(
                    player.getLocation(),
                    Config.ABILITY__CREEPER__POWER.getFloat(),
                    Config.ABILITY__CREEPER__FIRE.getBoolean());

            // remove hat
            HatManager.getManager().removeHat(player);
        }

        @Override
        public void cancel() {
            // stop blinking
            blinkTask.cancel();
            blinkTask = null;

            // remove task from memory
            ((CreeperAbility) mobHat.getAbility()).explodeTask = null;

            super.cancel();
        }
    }

    /**
     * Make the player blink like a creeper about to explode
     */
    private class Blink extends BukkitRunnable {
        private final Player player;
        private boolean glow = false;

        Blink(Player player) {
            this.player = player;
        }

        @Override
        public void run() {
            player.setGlowing(glow = !glow);
        }

        @Override
        public void cancel() {
            player.setGlowing(false);

            super.cancel();
        }
    }
}
