package net.downwithdestruction.mobhats.hat;

import java.util.Collections;
import java.util.HashSet;
import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.configuration.Config;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

/**
 * Represents an Enderman type MobHat
 * <p>
 * Warning: Teleport with passengers is currently bugged.
 * No way to detect when teleport attempted so cant eject
 * https://hub.spigotmc.org/jira/browse/SPIGOT-2064
 */
public class EndermanHat extends MobHat {
    /**
     * Constructs a new Enderman type MobHat for player
     *
     * @param player Player
     */
    public EndermanHat(Player player) {
        super(HatType.ENDERMAN, player);
        setAbility(new EndermanAbility());
    }

    /**
     * Enderman ability class.
     * <p>
     * This class holds the event listeners for the EndermanHat
     */
    private class EndermanAbility extends Ability {
        /**
         * Teleport to where looking when right clicking
         *
         * @param event PlayerInteractEvent
         */
        @Override
        public void onPlayerInteract(PlayerInteractEvent event) {
            if (event.getHand() != EquipmentSlot.HAND) {
                return; // only listen to one hand
            }

            if (!(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
                return; // not right clicking
            }

            Player player = event.getPlayer();
            Block block = player.getTargetBlock(
                    new HashSet<Material>(Collections.singletonList(Material.AIR)),
                    Config.ABILITY__ENDERMAN__MAX_TELEPORT_DISTANCE.getInt());

            if (block == null) {
                return; // no block in sight
            }

            // PlayerListener will handle passenger ejection
            player.teleport(block.getLocation().add(0.5, 1.1, 0.5));
        }
    }
}
