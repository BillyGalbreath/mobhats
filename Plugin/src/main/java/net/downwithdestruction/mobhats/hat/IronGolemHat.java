package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.configuration.Config;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * Represents a Iron Golem type MobHat
 */
public class IronGolemHat extends MobHat {
    /**
     * Constructs a new Iron Golem type MobHat for player
     *
     * @param player Player
     */
    public IronGolemHat(Player player) {
        super(HatType.IRON_GOLEM, player);
        setAbility(new IronGolemAbility());
    }

    @Override
    public void onAttach() {
        // set higher max health
        ((IronGolemAbility) getAbility())
                .setMaxHealth(getPlayer());
    }

    @Override
    public void onDetach() {
        // set normal player max health
        ((IronGolemAbility) getAbility())
                .setNormalHealth(getPlayer());
    }

    /**
     * Iron Golem ability class.
     * <p>
     * This class holds the event listeners for the IronGolemHat
     */
    private class IronGolemAbility extends Ability {
        /**
         * Reset the maximum health on player respawn
         */
        @Override
        public void onPlayerRespawn(PlayerRespawnEvent event) {
            setMaxHealth(event.getPlayer());
        }

        /**
         * Sets the player's max health to that of Iron Golem
         *
         * @param player Player
         */
        public void setMaxHealth(Player player) {
            player.setMaxHealth(Config.ABILITY__IRON_GOLEM__MAX_HEALTH.getDouble());
        }

        /**
         * Sets the player's max health back to normal
         *
         * @param player Player
         */
        public void setNormalHealth(Player player) {
            player.setMaxHealth(20.0D);
        }
    }
}
