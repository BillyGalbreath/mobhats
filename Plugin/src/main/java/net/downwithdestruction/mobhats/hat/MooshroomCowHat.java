package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Mushroom Cow type MobHat
 */
public class MooshroomCowHat extends MobHat {
    /**
     * Constructs a new Mushroom Cow type MobHat for player
     *
     * @param player Player
     */
    public MooshroomCowHat(Player player) {
        super(HatType.MOOSHROOM_COW, player);
        setAbility(new MooshroomCowAbility());
    }

    /**
     * Mushroom Cow ability class.
     * <p>
     * This class holds the event listeners for the MooshroomCowHat
     */
    private class MooshroomCowAbility extends Ability {
        //
    }
}
