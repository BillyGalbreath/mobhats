package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Represents a Ocelot type MobHat
 */
public class OcelotHat extends MobHat {
    /**
     * Constructs a new Ocelot type MobHat for player
     *
     * @param player Player
     */
    public OcelotHat(Player player) {
        super(HatType.OCELOT, player);
        setAbility(new OcelotAbility());
    }

    /**
     * Ocelot ability class.
     * <p>
     * This class holds the event listeners for the OcelotHat
     */
    private class OcelotAbility extends Ability {
        /**
         * Cancel fall damage
         *
         * @param event EntityDamageEvent
         */
        @Override
        public void onPlayerReceiveDamage(EntityDamageEvent event) {
            if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
                event.setCancelled(true);
            }
        }
    }
}
