package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;

/**
 * Represents a Siamese Cat type MobHat
 */
public class OcelotSiameseHat extends MobHat {
    /**
     * Constructs a new Siamese Cat type MobHat for player
     *
     * @param player Player
     */
    public OcelotSiameseHat(Player player) {
        super(HatType.OCELOT_SIAMESE, player);
        setAbility(new OcelotSiameseAbility());

        ((Ocelot) getEntity()).setCatType((Ocelot.Type) getType().getTypeData());
    }

    /**
     * Siamese Cat ability class.
     * <p>
     * This class holds the event listeners for the OcelotSiameseHat
     */
    private class OcelotSiameseAbility extends Ability {
        //
    }
}
