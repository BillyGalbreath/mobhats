package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;

/**
 * Represents a Tabby Cat type MobHat
 */
public class OcelotTabbyHat extends MobHat {
    /**
     * Constructs a new Tabby Cat type MobHat for player
     *
     * @param player Player
     */
    public OcelotTabbyHat(Player player) {
        super(HatType.OCELOT_TABBY, player);
        setAbility(new OcelotTabbyAbility());

        ((Ocelot) getEntity()).setCatType((Ocelot.Type) getType().getTypeData());
    }

    /**
     * Tabby Cat ability class.
     * <p>
     * This class holds the event listeners for the OcelotTabbyHat
     */
    private class OcelotTabbyAbility extends Ability {
        //
    }
}
