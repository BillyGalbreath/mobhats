package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;

/**
 * Represents a Tuxedo Cat type MobHat
 */
public class OcelotTuxedoHat extends MobHat {
    /**
     * Constructs a new Tuxedo Cat type MobHat for player
     *
     * @param player Player
     */
    public OcelotTuxedoHat(Player player) {
        super(HatType.OCELOT_TUXEDO, player);
        setAbility(new OcelotTuxedoAbility());

        ((Ocelot) getEntity()).setCatType((Ocelot.Type) getType().getTypeData());
    }

    /**
     * Tuxedo Cat ability class.
     * <p>
     * This class holds the event listeners for the OcelotTuxedoHat
     */
    private class OcelotTuxedoAbility extends Ability {
        //
    }
}
