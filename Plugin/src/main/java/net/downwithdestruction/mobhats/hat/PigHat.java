package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Pig type MobHat
 */
public class PigHat extends MobHat {
    /**
     * Constructs a new Pig type MobHat for player
     *
     * @param player Player
     */
    public PigHat(Player player) {
        super(HatType.PIG, player);
        setAbility(new PigAbility());
    }

    /**
     * Pig ability class.
     * <p>
     * This class holds the event listeners for the PigHat
     */
    private class PigAbility extends Ability {
        //
    }
}
