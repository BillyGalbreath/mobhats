package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Sheep type MobHat
 */
public class SheepHat extends MobHat {
    /**
     * Constructs a new Sheep type MobHat for player
     *
     * @param player Player
     */
    public SheepHat(Player player) {
        super(HatType.SHEEP, player);
        setAbility(new SheepAbility());
    }

    /**
     * Sheep ability class.
     * <p>
     * This class holds the event listeners for the SheepHat
     */
    private class SheepAbility extends Ability {
        //
    }
}
