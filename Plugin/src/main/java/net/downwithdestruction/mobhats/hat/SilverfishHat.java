package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Silverfish type MobHat
 */
public class SilverfishHat extends MobHat {
    /**
     * Constructs a new Silverfish type MobHat for player
     *
     * @param player Player
     */
    public SilverfishHat(Player player) {
        super(HatType.SILVERFISH, player);
        setAbility(new SilverfishAbility());
    }

    /**
     * Silverfish ability class.
     * <p>
     * This class holds the event listeners for the SilverfishHat
     */
    private class SilverfishAbility extends Ability {
        //
    }
}
