package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Skeleton type MobHat
 */
public class SkeletonHat extends MobHat {
    /**
     * Constructs a new Skeleton type MobHat for player
     *
     * @param player Player
     */
    public SkeletonHat(Player player) {
        super(HatType.SKELETON, player);
        setAbility(new SkeletonAbility());
    }

    /**
     * Skeleton ability class.
     * <p>
     * This class holds the event listeners for the SkeletonHat
     */
    private class SkeletonAbility extends Ability {
        //
    }
}
