package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;

/**
 * Represents a large Slime type MobHat
 */
public class SlimeLargeHat extends MobHat {
    /**
     * Constructs a new large Slime type MobHat for player
     *
     * @param player Player
     */
    public SlimeLargeHat(Player player) {
        super(HatType.SLIME_LARGE, player);
        setAbility(new SlimeLargeAbility());

        ((Slime) getEntity()).setSize((int) getType().getTypeData());
    }

    /**
     * Large Slime ability class.
     * <p>
     * This class holds the event listeners for the SlimeLargeHat
     */
    private class SlimeLargeAbility extends Ability {
        //
    }
}
