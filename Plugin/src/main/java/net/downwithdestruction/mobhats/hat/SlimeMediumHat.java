package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;

/**
 * Represents a mediuam Slime type MobHat
 */
public class SlimeMediumHat extends MobHat {
    /**
     * Constructs a new medium Slime type MobHat for player
     *
     * @param player Player
     */
    public SlimeMediumHat(Player player) {
        super(HatType.SLIME_MEDIUM, player);
        setAbility(new SlimeMediumAbility());

        ((Slime) getEntity()).setSize((int) getType().getTypeData());
    }

    /**
     * Medium Slime ability class.
     * <p>
     * This class holds the event listeners for the SlimeMediumHat
     */
    private class SlimeMediumAbility extends Ability {
        //
    }
}
