package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;

/**
 * Represents a small Slime type MobHat
 */
public class SlimeSmallHat extends MobHat {
    /**
     * Constructs a new small Slime type MobHat for player
     *
     * @param player Player
     */
    public SlimeSmallHat(Player player) {
        super(HatType.SLIME, player);
        setAbility(new SlimeSmallAbility());

        ((Slime) getEntity()).setSize((int) getType().getTypeData());
    }

    /**
     * Small Slime ability class.
     * <p>
     * This class holds the event listeners for the SlimeSmallHat
     */
    private class SlimeSmallAbility extends Ability {
        //
    }
}
