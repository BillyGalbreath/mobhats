package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.configuration.Config;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.EntityBlockFormEvent;

/**
 * Represents a Snowman type MobHat
 */
public class SnowmanHat extends MobHat {
    /**
     * Constructs a new Snowman type MobHat for player
     *
     * @param player Player
     */
    public SnowmanHat(Player player) {
        super(HatType.SNOWMAN, player);
        setAbility(new SnowmanAbility());
    }

    /**
     * Snowman ability class.
     * <p>
     * This class holds the event listeners for the SnowmanHat
     */
    private class SnowmanAbility extends Ability {
        /**
         * Stop snowman from making snow trail on the ground
         */
        @Override
        public void onEntityBlockForm(EntityBlockFormEvent event) {
            if (Config.ABILITY__SNOWMAN__LEAVE_TRAIL.getBoolean()) {
                return; // allow snowman trail
            }

            if (event.getEntity().getType() != EntityType.SNOWMAN) {
                return; // should not happen
            }

            if (event.getNewState().getType() != Material.SNOW) {
                return; // did not create snow layer
            }

            // cancel snowman making snow trail
            event.setCancelled(true);
        }
    }
}
