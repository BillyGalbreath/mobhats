package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Spider type MobHat
 */
public class SpiderHat extends MobHat {
    /**
     * Constructs a new Spider type MobHat for player
     *
     * @param player Player
     */
    public SpiderHat(Player player) {
        super(HatType.SPIDER, player);
        setAbility(new SpiderAbility());
    }

    /**
     * Spider ability class.
     * <p>
     * This class holds the event listeners for the SpiderHat
     */
    private class SpiderAbility extends Ability {
        //
    }
}
