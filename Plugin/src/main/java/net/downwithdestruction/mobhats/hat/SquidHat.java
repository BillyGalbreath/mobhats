package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Represents a Squid type MobHat
 */
public class SquidHat extends MobHat {
    private AirTick airTickTask;

    /**
     * Constructs a new Squid type MobHat for player
     *
     * @param player Player
     */
    public SquidHat(Player player) {
        super(HatType.SQUID, player);
        setAbility(new SquidAbility());
    }

    @Override
    public void onAttach() {
        if (airTickTask != null) {
            // should not already be running
            // cancel if any are found
            airTickTask.cancel();
        }
        airTickTask = new AirTick(getPlayer());
        airTickTask.runTaskTimer(MobHats.getPlugin(), 1, 1);
    }

    @Override
    public void onDetach() {
        if (airTickTask == null) {
            return; // no task is running
        }
        airTickTask.cancel();
        airTickTask = null;
    }

    /**
     * Squid ability class.
     * <p>
     * This class holds the event listeners for the SquidHat
     */
    private class SquidAbility extends Ability {
        @Override
        public void onPlayerReceiveDamage(EntityDamageEvent event) {
            if (event.getCause() == EntityDamageEvent.DamageCause.DROWNING) {
                event.setCancelled(true);
            }
        }
    }

    /**
     * This increments the air tick timer while the task is running
     * to give the appearance of regaining air instead of losing it
     */
    private class AirTick extends BukkitRunnable {
        private final Player player;
        private final int max;

        AirTick(Player player) {
            this.player = player;
            max = player.getMaximumAir();
        }

        @Override
        public void run() {
            // increment by 2 to battle the -1 from server
            int air = player.getRemainingAir() + 2;

            // make sure we are not setting more than maximum
            // to prevent more than 10 bubbles from appearing
            // on the client's screen
            if (air > max) {
                air = max;
            }

            // set the calculated air value
            player.setRemainingAir(air);
        }
    }
}
