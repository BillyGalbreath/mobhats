package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;

/**
 * Represents a Wither Skeleton type MobHat
 * <p>
 * Warning: Wither Skeletons flicker in sunlight of overworld.
 * https://bugs.mojang.com/browse/MC-26690
 */
public class WitherSkeletonHat extends MobHat {
    /**
     * Constructs a new Wither Skeleton type MobHat for player
     *
     * @param player Player
     */
    public WitherSkeletonHat(Player player) {
        super(HatType.WITHER_SKELETON, player);
        setAbility(new WitherSkeletonAbility());

        ((Skeleton) getEntity()).setSkeletonType((Skeleton.SkeletonType) getType().getTypeData());
    }

    /**
     * Wither Skeleton ability class.
     * <p>
     * This class holds the event listeners for the WitherSkeletonHat
     */
    private class WitherSkeletonAbility extends Ability {
        //
    }
}
