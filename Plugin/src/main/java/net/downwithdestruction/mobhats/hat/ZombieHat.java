package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Zombie type MobHat
 */
public class ZombieHat extends MobHat {
    /**
     * Constructs a new Zombie type MobHat for player
     *
     * @param player Player
     */
    public ZombieHat(Player player) {
        super(HatType.ZOMBIE, player);
        setAbility(new ZombieAbility());
    }

    /**
     * Zombie ability class.
     * <p>
     * This class holds the event listeners for the ZombieHat
     */
    private class ZombieAbility extends Ability {
        //
    }
}
