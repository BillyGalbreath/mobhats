package net.downwithdestruction.mobhats.hat;

import net.downwithdestruction.mobhats.api.Ability;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import org.bukkit.entity.Player;

/**
 * Represents a Zombie Pigman type MobHat
 */
public class ZombiePigmanHat extends MobHat {
    /**
     * Constructs a new Zombie Pigman type MobHat for player
     *
     * @param player Player
     */
    public ZombiePigmanHat(Player player) {
        super(HatType.ZOMBIE_PIGMAN, player);
        setAbility(new ZombiePigmanAbility());
    }

    /**
     * Zombie Pigman ability class.
     * <p>
     * This class holds the event listeners for the ZombiePigmanHat
     */
    private class ZombiePigmanAbility extends Ability {
        //
    }
}
