package net.downwithdestruction.mobhats.listener;

import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

/**
 * Bukkit listener for abilities
 */
public class AbilityListener implements Listener {
    @EventHandler
    public void onEntityBlockForm(EntityBlockFormEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getEntity());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onEntityBlockForm(event);
        }
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerChangedWorld(event);
        }
    }

    @EventHandler
    public void onPlayerHitEntity(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) {
            return; // only fire for players doing damage
        }
        MobHat mobHat = HatManager.getManager().getHat(event.getDamager());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerHitEntity(event);
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerInteract(event);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerMove(event);
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerTeleport(event);
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerRespawn(event);
        }
    }

    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerToggleFlight(event);
        }
    }

    @EventHandler
    public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerToggleSneak(event);
        }
    }

    @EventHandler
    public void onPlayerReceiveDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return; // not a player
        }
        MobHat mobHat = HatManager.getManager().getHat((Player) event.getEntity());
        if (mobHat != null && mobHat.getAbility() != null) {
            mobHat.getAbility().onPlayerReceiveDamage(event);
        }
    }
}
