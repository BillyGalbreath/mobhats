package net.downwithdestruction.mobhats.listener;

import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import net.downwithdestruction.mobhats.api.nms.Ride;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class MobHatListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if (!event.isCancelled()) {
            return; // not cancelled. ignore
        }

        MobHat mobHat = HatManager.getManager().getHat(event.getEntity());
        if (mobHat == null) {
            return; // not a hat
        }

        //uncancel event
        event.setCancelled(false);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerHitEntity(EntityDamageByEntityEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getEntity());
        if (mobHat == null) {
            return; // not a hat
        }

        // do not damage hat entities
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMobHatDetached(VehicleExitEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getExited());
        if (mobHat == null) {
            System.out.println("Exit allowed (not a hat)");
            return; // not a hat
        }

        Material material = event.getVehicle().getLocation().getBlock().getType();
        if (!(material == Material.WATER || material == Material.STATIONARY_WATER)) {
            material = event.getExited().getLocation().getBlock().getType();
            if (!(material == Material.WATER || material == Material.STATIONARY_WATER)) {
                System.out.println("Exit allowed (not in water)");
                return; // not in water
            }
        }

        // cancel detachment
        System.out.println("Exit cancelled");
        event.setCancelled(true);

        // reattach (just in case)
        Ride.getInstance().startRiding(event.getVehicle(), event.getExited());
    }
}
