package net.downwithdestruction.mobhats.listener;

import net.downwithdestruction.mobhats.MobHats;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import net.downwithdestruction.mobhats.api.nms.NMS;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Player listener for MobHat controls
 */
public class PlayerListener implements Listener {
    /**
     * Fix hat rotation when player moves
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        MobHat mobHat = HatManager.getManager().getHat(player);
        if (mobHat == null) {
            return; // not wearing mobhat
        }
        float yaw = event.getTo().getYaw();
        NMS nms = NMS.getInstance();
        nms.setRotation(mobHat.getHolder(), yaw);
        nms.setRotation(mobHat.getEntity(), yaw);
    }

    /**
     * Remove hat when player quits
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        HatManager.getManager().removeHat(event.getPlayer());
    }

    /**
     * Despawn hat entities when player dies (respawn it when player respawns)
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getEntity());
        if (mobHat == null) {
            return; // player not wearing a hat
        }
        HatManager.getManager().despawnEntities(mobHat);
    }

    /**
     * Spawn hat entities when player respawns
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat == null) {
            return; // player not wearing a hat
        }
        HatManager.getManager().spawnEntities(mobHat);
    }

    /**
     * Remove hat entities when player teleports and respawn afterwards
     * <p>
     * This does not currently function correctly. Waiting on bugfix from
     * Spigot team: SPIGOT-2064
     * <p>
     * https://hub.spigotmc.org/jira/browse/SPIGOT-2064
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat == null) {
            return; // player not wearing a hat
        }
        HatManager.getManager().despawnEntities(mobHat);
        Bukkit.getScheduler().runTaskLater(MobHats.getPlugin(),
                () -> HatManager.getManager().spawnEntities(mobHat), 5);
    }

    /**
     * Remove MobHat when player changes worlds
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        MobHat mobHat = HatManager.getManager().getHat(event.getPlayer());
        if (mobHat == null) {
            return; // player not wearing a hat
        }
        HatManager.getManager().removeHat(event.getPlayer());
    }
}
