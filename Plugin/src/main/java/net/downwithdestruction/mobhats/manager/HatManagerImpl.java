package net.downwithdestruction.mobhats.manager;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import net.downwithdestruction.mobhats.Chat;
import net.downwithdestruction.mobhats.api.HatType;
import net.downwithdestruction.mobhats.api.MobHat;
import net.downwithdestruction.mobhats.api.manager.HatManager;
import net.downwithdestruction.mobhats.api.nms.NBTTag;
import net.downwithdestruction.mobhats.api.nms.NMS;
import net.downwithdestruction.mobhats.api.nms.Ride;
import net.downwithdestruction.mobhats.configuration.Config;
import net.downwithdestruction.mobhats.configuration.Lang;
import net.downwithdestruction.mobhats.hat.BatHat;
import net.downwithdestruction.mobhats.hat.BlazeHat;
import net.downwithdestruction.mobhats.hat.CaveSpiderHat;
import net.downwithdestruction.mobhats.hat.ChickenHat;
import net.downwithdestruction.mobhats.hat.CowHat;
import net.downwithdestruction.mobhats.hat.CreeperHat;
import net.downwithdestruction.mobhats.hat.EndermanHat;
import net.downwithdestruction.mobhats.hat.IronGolemHat;
import net.downwithdestruction.mobhats.hat.MooshroomCowHat;
import net.downwithdestruction.mobhats.hat.OcelotHat;
import net.downwithdestruction.mobhats.hat.OcelotSiameseHat;
import net.downwithdestruction.mobhats.hat.OcelotTabbyHat;
import net.downwithdestruction.mobhats.hat.OcelotTuxedoHat;
import net.downwithdestruction.mobhats.hat.PigHat;
import net.downwithdestruction.mobhats.hat.SheepHat;
import net.downwithdestruction.mobhats.hat.SilverfishHat;
import net.downwithdestruction.mobhats.hat.SkeletonHat;
import net.downwithdestruction.mobhats.hat.SlimeLargeHat;
import net.downwithdestruction.mobhats.hat.SlimeMediumHat;
import net.downwithdestruction.mobhats.hat.SlimeSmallHat;
import net.downwithdestruction.mobhats.hat.SnowmanHat;
import net.downwithdestruction.mobhats.hat.SpiderHat;
import net.downwithdestruction.mobhats.hat.SquidHat;
import net.downwithdestruction.mobhats.hat.WitherSkeletonHat;
import net.downwithdestruction.mobhats.hat.ZombieHat;
import net.downwithdestruction.mobhats.hat.ZombiePigmanHat;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.pl3x.bukkit.chatapi.ComponentSender;
import net.pl3x.bukkit.titleapi.Title;
import net.pl3x.bukkit.titleapi.api.TitleType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * MobHat Manager
 */
public class HatManagerImpl extends HatManager {
    @Override
    public void setHat(Player player, HatType hatType) {
        removeHat(player);
        MobHat mobHat = spawnHat(hatType, player);
        mobHats.put(player.getUniqueId(), mobHat);

        // call the attachment listener
        mobHat.onAttach();

        new Title(TitleType.RESET, null)
                .send(player);
        new Title(Config.TITLE__FADE_IN.getInt(),
                Config.TITLE__STAY.getInt(),
                Config.TITLE__FADE_OUT.getInt())
                .send(player);
        new Title(TitleType.TITLE, Lang.HAT_ATTACHED_TITLE
                .replace("{player}", player.getName())
                .replace("{type}", hatType.getName())
                .replace("{ability}", hatType.getAbilityDescription()))
                .send(player);
        new Title(TitleType.SUBTITLE, Lang.HAT_ATTACHED_SUBTITLE
                .replace("{player}", player.getName())
                .replace("{type}", hatType.getName())
                .replace("{ability}", hatType.getAbilityDescription()))
                .send(player);
        ComponentSender.sendMessage(player, ChatMessageType.ACTION_BAR,
                new TextComponent(BaseComponent.toLegacyText(
                        TextComponent.fromLegacyText(Lang.HAT_ATTACHED_ACTIONBAR
                                .replace("{player}", player.getName())
                                .replace("{type}", hatType.getName())
                        ))));
        new Chat(Lang.HAT_ATTACHED_CHAT
                .replace("{player}", player.getName())
                .replace("{type}", hatType.getName()))
                .send(player);
    }

    @Override
    public void removeHat(Player player) {
        MobHat mobHat = mobHats.remove(player.getUniqueId());
        if (mobHat != null) {
            despawnEntities(mobHat);

            // call the detachment listener
            mobHat.onDetach();
        }
    }

    @Override
    public void removeAll() {
        for (Iterator<Map.Entry<UUID, MobHat>> it = mobHats.entrySet().iterator(); it.hasNext(); ) {
            MobHat mobHat = it.next().getValue();
            despawnEntities(mobHat);
            mobHat.onDetach();
            it.remove();
        }
    }

    @Override
    public MobHat getHat(Player player) {
        return mobHats.get(player.getUniqueId());
    }

    @Override
    public MobHat getHat(Entity entity) {
        UUID uuid = entity.getUniqueId();
        for (MobHat mobHat : mobHats.values()) {
            if (mobHat.getHolder().getUniqueId() == uuid) {
                return mobHat;
            }
            if (mobHat.getEntity().getUniqueId() == uuid) {
                return mobHat;
            }
        }
        return null;
    }

    @Override
    public void spawnEntities(MobHat mobHat) {
        Player player = mobHat.getPlayer();

        // spawn entities
        Entity holder = player.getWorld().spawnEntity(player.getLocation(), EntityType.PIG);
        Entity entity = player.getWorld().spawnEntity(player.getLocation(), mobHat.getType().getEntityType());

        // set entities
        mobHat.setEntity(entity);
        mobHat.setHolder(holder);

        // set behaviors
        Plugin plugin = Bukkit.getPluginManager().getPlugin("MobHats");
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            NMS nms = NMS.getInstance();
            nms.setInvisible(holder);
            nms.setSilent(holder);
            nms.setSilent(entity);
            nms.removeCollisionBox(holder);
            nms.removeCollisionBox(entity);
            float yaw = player.getLocation().getYaw();
            nms.setRotation(holder, yaw);
            nms.setRotation(entity, yaw);
        }, 2); // must wait 2 ticks after spawning entity for datawatcher

        // set nbt tags
        NBTTag nbtTag = NBTTag.getInstance();
        nbtTag.setTag(holder, "NoAI", true);
        nbtTag.setTag(entity, "NoAI", true); // TODO add back some AI features, like bat flapping wings
        holder.setInvulnerable(true);
        entity.setInvulnerable(true);

        // set metadata for easy tracking
        holder.setMetadata("MobHatEntity", new FixedMetadataValue(plugin, entity.getUniqueId()));
        holder.setMetadata("MobHatPlayer", new FixedMetadataValue(plugin, player.getUniqueId()));
        entity.setMetadata("MobHatHolder", new FixedMetadataValue(plugin, holder.getUniqueId()));
        entity.setMetadata("MobHatPlayer", new FixedMetadataValue(plugin, player.getUniqueId()));

        // attach entities
        Ride ride = Ride.getInstance();
        ride.startRiding(holder, entity);
        ride.startRiding(player, holder);

        // check if entities are still attached
        new BukkitRunnable() {
            @Override
            public void run() {
                if (player.getPassenger() != null &&
                        player.getPassenger().equals(holder) &&
                        holder.getPassenger() != null &&
                        holder.getPassenger().equals(entity)) {
                    return; // everything looks good.
                }

                HatManager.getManager().removeHat(player);
                new Chat(Lang.CANT_SET_HAT_HERE).send(player);
            }
        }.runTaskLater(plugin, 10);
    }

    @Override
    public void despawnEntities(MobHat mobHat) {
        Entity holder = mobHat.getHolder();
        Entity entity = mobHat.getEntity();
        Ride ride = Ride.getInstance();
        try {
            ride.stopRiding(entity);
            ride.stopRiding(holder);
            entity.remove();
            holder.remove();
        } catch (Exception ignore) {
        }
    }

    @Override
    public MobHat spawnHat(HatType hatType, Player player) {
        switch (hatType) {
            case BAT:
                return new BatHat(player);
            case BLAZE:
                return new BlazeHat(player);
            case CAVE_SPIDER:
                return new CaveSpiderHat(player);
            case CHICKEN:
                return new ChickenHat(player);
            case COW:
                return new CowHat(player);
            case CREEPER:
                return new CreeperHat(player);
            case ENDERMAN:
                return new EndermanHat(player);
            case IRON_GOLEM:
                return new IronGolemHat(player);
            case MOOSHROOM_COW:
                return new MooshroomCowHat(player);
            case OCELOT:
                return new OcelotHat(player);
            case OCELOT_SIAMESE:
                return new OcelotSiameseHat(player);
            case OCELOT_TABBY:
                return new OcelotTabbyHat(player);
            case OCELOT_TUXEDO:
                return new OcelotTuxedoHat(player);
            case PIG:
                return new PigHat(player);
            case SHEEP:
                return new SheepHat(player);
            case SILVERFISH:
                return new SilverfishHat(player);
            case SKELETON:
                return new SkeletonHat(player);
            case SLIME:
                return new SlimeSmallHat(player);
            case SLIME_MEDIUM:
                return new SlimeMediumHat(player);
            case SLIME_LARGE:
                return new SlimeLargeHat(player);
            case SNOWMAN:
                return new SnowmanHat(player);
            case SPIDER:
                return new SpiderHat(player);
            case SQUID:
                return new SquidHat(player);
            case WITHER_SKELETON:
                return new WitherSkeletonHat(player);
            case ZOMBIE:
                return new ZombieHat(player);
            case ZOMBIE_PIGMAN:
                return new ZombiePigmanHat(player);
            default:
                throw new IllegalArgumentException("Unknown HatType");
        }
    }
}
