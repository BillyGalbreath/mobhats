package net.downwithdestruction.mobhats.nms.v1_10_R1;

import net.downwithdestruction.mobhats.api.nms.NBTTag;
import net.minecraft.server.v1_10_R1.Entity;
import net.minecraft.server.v1_10_R1.EntityLiving;
import net.minecraft.server.v1_10_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

/**
 * NBTTag class nms handler
 */
public class NBTTagHandler extends NBTTag {
    @Override
    public void setTag(org.bukkit.entity.Entity entity, String tagName, boolean value) {
        Entity nmsEntity = ((CraftEntity) entity).getHandle();
        NBTTagCompound tag = new NBTTagCompound();
        nmsEntity.c(tag);
        tag.setBoolean(tagName, value);
        EntityLiving entityLiving = (EntityLiving) nmsEntity;
        entityLiving.a(tag);
    }

    @Override
    public ItemStack setTag(ItemStack item, EntityType value) {
        net.minecraft.server.v1_10_R1.ItemStack stack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound idTag = new NBTTagCompound();
        idTag.setString("id", value.getName());
        NBTTagCompound tag = new NBTTagCompound();
        tag.set("EntityTag", idTag);
        stack.setTag(tag);
        return CraftItemStack.asBukkitCopy(stack);
    }

    @Override
    public Boolean getTag(org.bukkit.entity.Entity entity, String tagName) {
        Entity nmsEntity = ((CraftEntity) entity).getHandle();
        NBTTagCompound tag = new NBTTagCompound();
        nmsEntity.c(tag);
        return tag.getBoolean(tagName);
    }
}
