package net.downwithdestruction.mobhats.nms.v1_10_R1;

import net.downwithdestruction.mobhats.api.nms.NMS;
import net.minecraft.server.v1_10_R1.AxisAlignedBB;
import net.minecraft.server.v1_10_R1.EntityLiving;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftEntity;
import org.bukkit.entity.Entity;

public class NMSHandler extends NMS {
    @Override
    public void setRotation(Entity entity, float yaw) {
        EntityLiving nmsEntity = (EntityLiving) ((CraftEntity) entity).getHandle();
        nmsEntity.pitch = 0;
        nmsEntity.lastPitch = 0;
        nmsEntity.yaw = yaw;
        nmsEntity.lastYaw = yaw;
        nmsEntity.aQ = yaw;
        nmsEntity.aR = yaw;
        nmsEntity.aO = yaw;
        nmsEntity.aP = yaw;
    }

    @Override
    public void setSilent(Entity passenger) {
        ((CraftEntity) passenger).getHandle().setSilent(true);
    }

    @Override
    public void setInvisible(Entity passenger) {
        ((CraftEntity) passenger).getHandle().setInvisible(true);
    }

    @Override
    public void removeCollisionBox(Entity entity) {
        ((CraftEntity) entity).getHandle().a(new AxisAlignedBB(0, 0, 0, 0, 0, 0));
    }
}
