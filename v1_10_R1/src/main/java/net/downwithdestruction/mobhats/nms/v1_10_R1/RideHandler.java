package net.downwithdestruction.mobhats.nms.v1_10_R1;

import net.downwithdestruction.mobhats.api.nms.Ride;
import net.minecraft.server.v1_10_R1.EntityPlayer;
import net.minecraft.server.v1_10_R1.PacketPlayOutMount;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Workaround Ride utility handler nms class
 */
public class RideHandler extends Ride {
    @Override
    public void startRiding(Entity vehicle, Entity passenger) {
        if (vehicle.setPassenger(passenger) && vehicle instanceof Player) {
            sendMountPacket((Player) vehicle);
        }
    }

    @Override
    public void stopRiding(Entity passenger) {
        Entity vehicle = passenger.getVehicle();
        if (vehicle == null) {
            return;
        }
        ejectPassengers(vehicle);
    }

    @Override
    public void ejectPassengers(Entity vehicle) {
        if (vehicle.eject() && vehicle instanceof Player) {
            sendMountPacket((Player) vehicle);
        }
    }

    private void sendMountPacket(Player player) {
        EntityPlayer playerHandle = ((CraftPlayer) player).getHandle();
        playerHandle.playerConnection.sendPacket(new PacketPlayOutMount(playerHandle));
    }
}
